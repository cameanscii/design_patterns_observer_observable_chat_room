package pl.sda.chatroom;

import java.util.Observable;
import java.util.Observer;

public class ChatRoom extends Observable {

    @Override
    public synchronized void addObserver(Observer o) {
        super.addObserver(o);
    }

    public void sentMessage(Object message){
        setChanged();
        notifyObservers(message);
    }


}
