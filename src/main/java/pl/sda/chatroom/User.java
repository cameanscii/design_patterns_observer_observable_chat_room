package pl.sda.chatroom;

import java.util.Observable;
import java.util.Observer;

public class User implements Observer {
    String name;

    public User(String name) {
        this.name = name;
    }

    public void update(Observable o, Object arg) {

        System.out.println(name + " informed about: "+arg);
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                '}';
    }
}
