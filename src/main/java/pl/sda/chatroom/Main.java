package pl.sda.chatroom;

public class Main {
    public static void main(String[] args) {


        User user1 = new User("Janusz");
        User user2 = new User("Rysiek");
        User user3 = new User("Zdzisiek");
        User user4 = new User("Marian");

        WebPortal webPortal = new WebPortal();

        ChatRoom chatRoom1 = new ChatRoom();
        ChatRoom chatRoom2 = new ChatRoom();

        webPortal.addChatRoom(chatRoom1);
        webPortal.addChatRoom(chatRoom2);
        webPortal.addUserToChatRoom(user1,chatRoom1);
        webPortal.addUserToChatRoom(user2,chatRoom1);
        webPortal.addUserToChatRoom(user3,chatRoom2);
        webPortal.addUserToChatRoom(user4,chatRoom2);

        webPortal.sendMessageToRoom("Siema", chatRoom1);
        webPortal.sendMessageToRoom("Dzień Dobry Panowie", chatRoom2);

    }
}
