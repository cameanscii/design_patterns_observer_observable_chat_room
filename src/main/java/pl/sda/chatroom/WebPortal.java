package pl.sda.chatroom;

import java.util.ArrayList;
import java.util.List;

public class WebPortal {
    private List<ChatRoom> chatRooms = new ArrayList<ChatRoom>();

    public void addChatRoom(ChatRoom chatRoom){
        chatRooms.add(chatRoom);
    }

    public void deleteChatRooms(ChatRoom chatRoom) {
       chatRooms.remove(chatRoom);
    }

    public void sendMessageToRoom(String string, ChatRoom chatRoom) {
        chatRoom.sentMessage(string);
    }

    public void addUserToChatRoom(User user, ChatRoom chatRoom){
        chatRoom.addObserver(user);
    }


    @Override
    public String toString() {
        return "WebPortal{" +
                "chatRooms=" + chatRooms +
                '}';
    }
}
