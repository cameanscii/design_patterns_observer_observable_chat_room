package pl.sda.chatroom_new_version_for_java_9;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;

public class ChatRoomNewVer {

    private ObjectProperty<String> messageObjectProperty = new SimpleObjectProperty<String>();

    public void addObserver(ChangeListener<String> stringChangeListener) {
        messageObjectProperty.addListener(stringChangeListener);
    }

    public void sentMessage(String message2) {
        messageObjectProperty.setValue(message2);
    }
}
