package pl.sda.chatroom_new_version_for_java_9;

import java.util.ArrayList;
import java.util.List;

public class WebPortalNewVer {
    private List<ChatRoomNewVer> chatRooms = new ArrayList<ChatRoomNewVer>();

    public void addChatRoom(ChatRoomNewVer chatRoomNewVer){
        chatRooms.add(chatRoomNewVer);
    }

    public void deleteChatRooms(ChatRoomNewVer chatRoomNewVer) {
        chatRooms.remove(chatRoomNewVer);
    }

    public void addUserToChatRoom(UserNewVer userNewVer, ChatRoomNewVer chatRoomNewVer){
        chatRoomNewVer.addObserver(userNewVer);
    }

    public void sendMessageToRoom(String string, ChatRoomNewVer chatRoomNewVer) {
        chatRoomNewVer.sentMessage(string);
    }
}
