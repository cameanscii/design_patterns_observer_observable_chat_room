package pl.sda.chatroom_new_version_for_java_9;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class UserNewVer implements ChangeListener<String> {
    String name;

    public UserNewVer(String name) {
        this.name = name;
    }


    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        System.out.println(name + " is informed about: " +newValue);
    }
}
